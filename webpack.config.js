const path 	  = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var combineLoaders = require('webpack-combine-loaders');
var clientdir =  path.join(__dirname,'client/');
var serverdir =  path.join(__dirname,'server/');

var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
});

const client = {
	devtool: 'eval',
 	entry: [
    	'webpack-dev-server/client?http://localhost:8080',
    	'webpack/hot/only-dev-server',
    	clientdir+"client.jsx"
  	],
	output:{
		path: path.join(__dirname,'dist/'),
		filename: "bundle.js",
		publicPath: '/'
	},
	module:{
		rules:
		[
		{
			test: /\.(js|jsx)$/,
		    loader: 'babel-loader',
		    exclude: /node_modules/,
		    query:{
		    	plugins:[
		    	'transform-class-properties',
		    	'transform-object-rest-spread'
		    	],
		    	presets:
		    	[	
		          ['env',{modules: false}],
		          ['react']
		        ]
		    } 
		},
		{
			test: /\.css$/,
      		loader: combineLoaders([
      		{
      			loader: 'style-loader'
      		},{
      			loader: 'css-loader',
      			query: {
      				modules: true,
      				localIdentName: '[name]__[local]___[hash:base64:5]'
      			}
      		}
      		])
      	}]
      },
      plugins: [
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
      ],
      resolve: {
      	extensions: ['.js', '.jsx','.css']
      }
  };

const server = {
	entry: {
		"build" : serverdir+"server.js"
	},
	target: "node",
	externals: nodeModules,
	devtool: "cheap-module-source-map",
	output:{
		path: path.join(__dirname,'dist/'),
		filename: "bundle.js"
	},
	module:{
		rules:
		[
		{
			test: /\.(js|jsx)$/,
		    loader: 'babel-loader',
		    exclude: /node_modules/,
		    query:{
		    	presets:
		    	[	
		          ['env',{modules: false}],
		          ['react']
		        ]
		    } 
		},
		{
        	test: /\.css$/,
        	use: ExtractTextPlugin.extract({
          	fallback: "style-loader",
          	use: "css-loader"
        	})
      	}
		]
	},
	plugins: [
    	new ExtractTextPlugin("styles.css"),
    	
  	],
	resolve: {
		extensions: ['.js', '.jsx','.css']
	}
};
new webpack.DefinePlugin({
  'process.env.NODE_ENV': JSON.stringify('development')
}),
module.exports = [client];
