import React,{Component} from 'react';
import { Switch, Route } from 'react-router-dom';

import Artist from './Artist.jsx';
import FullArtist from './FullArtist.jsx';		

class ArtistRoute extends Component{

	render(){
		return(
			<Switch>
				<Route path='/redux/artists/' exact component={Artist} />
				<Route path='/redux/artists/:name' component={FullArtist} />
			</Switch>
			)
	}

}
export default ArtistRoute;