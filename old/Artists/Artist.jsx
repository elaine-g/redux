import React,{Component} from 'react';
import { Link } from 'react-router-dom';
//import {ArtistConstant as Constants} from '../../constants/Constants.jsx';
import ArtistStore from '../../stores/ArtistStore.jsx';
import ArtistActionCreators from '../../actions/ArtistActionCreators.jsx';

class Artist extends Component{

	constructor(props){
		super(props);
		this.state = { 
			img : 'https://i.pinimg.com/564x/02/e9/25/02e9255f8bf5e883755ed138dc68e7bf.jpg',
			name: ArtistStore.getState().name,
			job : ArtistStore.getState().job,
			age: ArtistStore.getState().age,
			isShow: ArtistStore.getState().isShow
		};
	}

	componentDidMount(){
		this.unsubscribe = ArtistStore.subscribe(() => {
		this.setState({ 
			img: ArtistStore.getState().img,
			name: ArtistStore.getState().name,
			job : ArtistStore.getState().job,
			age: ArtistStore.getState().age,
			isShow: ArtistStore.getState().isShow
			});			
		});

	}

	render(){
		const markup = <div>
			<h1>Artists page! { this.state.job } </h1>
			<button onClick={ () => ArtistStore.dispatch(ArtistActionCreators.showInformation(!this.state.isShow)) }>show information</button>
			<button onClick={ () => ArtistStore.dispatch(ArtistActionCreators.changeAge(31)) }>Change age</button>
			
			<img src={this.state.img} />
			<div>
			{

				(() =>{
					if(!this.state.age || !this.state.isShow){
						return null;
					}else{
						if(this.state.isShow){
							return <p>{this.state.age}</p>;
						}
					}
				})()
			}
			</div>
			<br />
			<Link to={'/redux/artists/hayley-williams'}>Hayley williams</Link>
			</div>;
		return (
			markup 
		)
	}
}

export default Artist;