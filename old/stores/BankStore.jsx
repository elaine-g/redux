import {createStore} from 'redux';
import BankReducer from '../reducers/BankReducer.jsx';
const BankStore = createStore(BankReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
export default BankStore;