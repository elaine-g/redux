import React,{ Component } from 'react';
import BankStore from '../../stores/BankStore.jsx';
import BankActionCreators from '../../actions/BankActionCreators.jsx';
class BankApp extends Component{
	constructor(props){
		super(props);	
		this.state = { balance : BankStore.getState().balance };
		this.alertMe = this.alertMe.bind(this);
	}

	componentDidMount(){		
		this.unsubscribe = BankStore.subscribe(() =>{
			this.setState({
				balance: BankStore.getState().balance,
				modified: BankStore.getState().modified
			});
		});
	}
	componentWillUnmount() {
		BankStore.dispatch(BankActionCreators.resetAccount());
		this.unsubscribe();
	}
	alertMe(){
		BankStore.dispatch(BankActionCreators.depositIntoAccount(100));
	}
	render(){
		const owe = <div> 
		U need pay! you owe { this.state.balance }
		<button onClick={ () => BankStore.dispatch(BankActionCreators.depositIntoAccount(100)) }> clic</button>
		</div>;
		const initialMarkup = <div>
			<p> Fondos actuales {this.state.balance} </p>
			<button onClick={ () => BankStore.dispatch(BankActionCreators.depositIntoAccount(100)) }> clic</button>
			<button onClick={ () => BankStore.dispatch(BankActionCreators.withdrawAccount(10)) }>Retirar dinero</button>
			</div>;
		const markupModified = <p>State has been modified!</p>;
		if(this.state.balance < 0){
			return(
				owe
			)				
		}else{
			return(
				initialMarkup
			)
		}
	}
};

export default BankApp;