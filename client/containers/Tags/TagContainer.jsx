import React,{ Component } from 'react'
import { connect } from 'react-redux'
import TagActionCreators from '../../actions/TagActionCreators.js'

class TagContainer extends Component {

	constructor(props) {
	  super(props);
	}

	static requestInitialData(req){
		return TagActionCreators.fetchTag(req)
	}

	componentDidMount(){
		const { currentTag, tags,lastTag } = this.props
		if(currentTag && Array.isArray(tags) && tags.length <= 0 || lastTag !== currentTag){
			this.props.dispatch(TagContainer.requestInitialData(currentTag))
		}
	}

	componentWillReceiveProps(nextProps){
		if(this.props.match.params.tag !== nextProps.match.params.tag){
			this.props.dispatch(TagContainer.requestInitialData(nextProps.match.params.tag))
		}
	}

	render(){
		if(Array.isArray(this.props.tags) && this.props.tags.length >0 && !this.props.error.status){
			return(
				<div>
				{
					this.props.tags && this.props.tags.map((value,index) => {
						return(
							<p>{ value.title }</p>
							)
					})
				}
				</div>
				)
		}else if(this.props.error.status){
			return(
				<div>
					<p>{ this.props.error.message }</p>
				</div>
				)
		}else{
			return(
				<p>Tags</p>
			)			
		}
	}
}

const mapStateToProps = (state,props) =>{
	return{
		loading : state.TagReducer.ui.loading,
		tags: state.TagReducer.result,
		currentTag : props.match.params.tag,
		lastTag : state.TagReducer.lastTag,
		error: state.TagReducer.error
	}
}

export default connect(mapStateToProps)(TagContainer)