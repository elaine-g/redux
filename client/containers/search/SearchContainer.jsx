import React,{ Component } from 'react'
import { connect } from 'react-redux'
import queryString from 'query-string'

import SearchActionCreators from '../../actions/SearchActionCreators'

class SearchContainer extends Component{
	
	constructor(props) {
	  super(props);
	}

	componentDidMount(){
		const filters = {};
		const parsed = queryString.parse(this.props.location.search)
		const query = this.props.match.params.query

		if(parsed.order !== 'undefined' && query.length > 0){
			filters.order = parsed.order;
			this.props.search(query,filters);
		}
	}

	render(){
		const {keyword,loading,data,error} = this.props;
		if(loading){
			return(
				<div>
					Buscando ...
				</div>
				)
		}else{
			if(error.status && error.message.length > 0){
				return(
					<div>
						Error de búsqueda : { error.message }
					</div>
					)
			}else{
				return(
					<div>
					{
						data && data.results.map((value,index) => {
							return(
								<p key={index}>
									{ value.title }
								</p>
								)
						})
					}
					</div>
					)
			}
		}

	}

}

const mapStateToProps = (state,props) =>{
	return{
		keyword : props.match.params.query,
		loading : state.SearchReducer.ui.loading,
		data    : state.SearchReducer.result,
		error:{
			status  : state.SearchReducer.error.status,
			message : state.SearchReducer.error.message
		}
	}
};
const mapDispatchToProps = (dispatch) =>{
	return {
		search: (query,filters) => dispatch( SearchActionCreators.searchQuery(query,filters), dispatch ),
		dispatch
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(SearchContainer);
