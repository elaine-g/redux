import React,{ Component } from 'react'
import { connect } from 'react-redux'
import HomeFeaturedContainer from './HomeFeaturedContainer'

class HomeContainer extends Component{

	constructor(props) {
	  super(props);
	}

	componentDidMount(){

	}

	render(){
		return(
			<div>
				<HomeFeaturedContainer />
			</div>
		)
		
	}


}

export default connect(null)(HomeContainer);