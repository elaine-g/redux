import React,{ Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';

import HomeActionCreators from '../../actions/HomeActionCreators'

import HomeComponentFeaturedLeft from '../../components/Home/Featured/HomeComponentFeaturedLeft'
import HomeComponentFeaturedRight from '../../components/Home/Featured/HomeComponentFeaturedRight'

import ReactPlaceholder from 'react-placeholder'
import { splashScreenHomeFeaturedLoader } from '../../utils/loaders' 
import FeaturedContainerFailure from '../../components/Errors/Containers/FeaturedContainerFailure'
class HomeFeaturedContainer extends Component{
	
	constructor(props) {
	  super(props);

	}

	handleSplashScreen = () =>{
		const {featuredPostUi } = this.props;
		if(featuredPostUi.loading){
			return <div>{splashScreenHomeFeaturedLoader}</div>;
		}
		return null
	};

	handleShowComponent = () =>{
		let showComponent;
		const { featuredPostUi,featuredPosts,articlesErrorStatus,articlesErrorMessage,articlesErrorCode } = this.props;
		if(articlesErrorStatus && !featuredPostUi.loading){
			showComponent =  <FeaturedContainerFailure retry={this.props.fetchFeatured} error={true} errorMessage={articlesErrorMessage} code={articlesErrorCode} />
		}else if(!featuredPostUi.loading && featuredPosts){
			showComponent = <div>
							<HomeComponentFeaturedLeft posts={featuredPosts.leftSide} />
							<HomeComponentFeaturedRight posts={featuredPosts.rightSide} />
							</div>
							;
		}
		return showComponent;
	};

	static requestInitialData(){
		return HomeActionCreators.fetchFeatured()
	}

	componentDidMount(){
		if(!this.props.featuredPosts || Math.floor(Date.now() / 1000) > this.props.time){
			this.props.dispatch(HomeFeaturedContainer.requestInitialData())
		}
	}

	render(){

		return(
			<div>
				{this.handleSplashScreen()}
				{this.handleShowComponent()}
			</div>
			)
	}
}

const mapStateToProps = (state,props) => {
	return {
		featuredPosts: state.HomeReducer.featuredPost.data.featured,
		time: state.HomeReducer.featuredPost.data.time,
		articlesErrorStatus: state.HomeReducer.featuredPost.featuredError.status,
		articlesErrorMessage: state.HomeReducer.featuredPost.featuredError.message,
		articlesErrorCode: state.HomeReducer.featuredPost.featuredError.code,
		featuredPostUi: state.HomeReducer.featuredPost.ui,
	};
}
const mapDispatchToProps = (dispatch,props) => {
	return{
		fetchFeatured : () => dispatch(HomeActionCreators.fetchFeatured(),dispatch),
		dispatch
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeFeaturedContainer);