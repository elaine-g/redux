import React,{ Component } from 'react'
import { connect} from 'react-redux'
import PostAPI from '../../api/PostAPI'
import PostComponent from '../../components/Post/PostComponent'
import PostRelated from '../../components/Post/PostRelated'
import PostActionCreators,{ reset } from '../../actions/PostActionCreators'
import { bindActionCreators } from 'redux';

class PostContainer extends Component{

	constructor(props) {
	  super(props);

	}
	
	static requestInitialData(req){
		console.log('requesting post...' + req.year + '-' + req.month + '-' + req.slug)
		return PostActionCreators.fetchPost(req);	
	};

	isFeched = () => {
		let post;
		const {id,title,content,description} = this.props.post;
		const {loading} = this.props;
		if(typeof id === 'undefined' || typeof title === 'undefined' || typeof content === 'undefined' || typeof description === 'undefined'){
			post = false;
		}else if(!loading && typeof id !== 'undefined' && typeof title !== 'undefined' && typeof content !== 'undefined' && typeof description !== 'undefined'){
			post = this.props.post;
		}
		return post;
	};

	checkFailure = () => {
		const { status,message } = this.props.failure.error
		let errorMessage
		if(status === true){
			if(typeof message !== 'undefined'){
				errorMessage = 
				<div>{ message }</div>
			}else{
				errorMessage = <div>Unknow message</div> 
			}
		}
		return errorMessage
	};

	componentDidMount(){
		//this.props.fetchPost()
		const { dispatch } = this.props
		if(this.props.lastRequest !== this.props.match.params.slug  && !this.props.post.content){
			let url = {};
			url.year  = this.props.match.params.year
			url.month = this.props.match.params.month
			url.slug  = this.props.match.params.slug
			
			dispatch(PostContainer.requestInitialData(url));
		}
	};

	componentWillUnmount(){
		//this.props.dispatch(reset());
	};

	componentWillReceiveProps(nextProps){
		const { dispatch } = this.props
		if(this.props.match.url !== nextProps.match.url){
			let url = {};
			url.year  = nextProps.match.params.year
			url.month = nextProps.match.params.month
			url.slug  = nextProps.match.params.slug
			dispatch(PostContainer.requestInitialData(url));
		}else{
			console.log('same')
		}
	};

	render(){
		const actions = bindActionCreators(PostActionCreators, this.props.dispatch);
		return(
			<div>
			{
				this.checkFailure()
			}
			<PostComponent failure= { this.props.failure }loading={ this.props.loading && !this.props.failure.error.status } post={ this.isFeched() } date={ this.props.date } {...actions} />
			</div>
			)
	};


}

const mapStateToProps = (state,props) => {
	return {
		post : state.PostReducer.post,
		date : state.PostReducer.date,
		failure : state.PostReducer.failure,
		loading : state.PostReducer.ui.loading,
		related : state.PostReducer.related,
		lastRequest : state.PostReducer.lastRequest
	}
}



export default connect(mapStateToProps)(PostContainer)