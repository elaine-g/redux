import { createStore,applyMiddleware,combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import analyticsMiddleware from '../utils/middlewares/analyticsMiddleware.jsx';
import transitionLoading from '../utils/middlewares/transitionLoading.jsx';
import rootReducer from '../reducers/Index.js';

const store = createStore(rootReducer,composeWithDevTools(applyMiddleware(thunk,analyticsMiddleware,transitionLoading)));

export default store;
