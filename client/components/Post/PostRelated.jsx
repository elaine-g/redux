import React,{ Component } from 'react'
import PropTypes from 'prop-types' 

class PostRelated extends Component{
	constructor(props) {
	  super(props);
	 
	}

	componentDidMount(){}

	render(){
	
		return(
			<div>
			Related Posts { this.props.related } 
			</div>
			)
	}
}

PostRelated.propTypes = {
	related : PropTypes.array.isRequired
}

export default PostRelated