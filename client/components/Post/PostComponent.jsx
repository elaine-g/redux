import React,{ Component } from 'react'
import ReactPlaceholder from 'react-placeholder';

import {PostComponentBody} from './PostComponentBody'
import PostComponentHeader from './PostComponentHeader'
import "../../../node_modules/react-placeholder/lib/reactPlaceholder.css";
import HomeContainerStyles from '../../styles/HomeContainer.css'

export default class PostComponent extends Component{

	constructor(props) {
	  super(props);
	  this.state = {};

	}

	isPostFetched = () =>{
		let post
		if(!this.props.failure.error.status && !this.props.loading && this.props.post !== false){
			const {id,title,content,description} = this.props.post
			post = 
			<div>
			<PostComponentHeader date={this.props.date} title={ title} /><PostComponentBody body={ content } />
			</div>
		}	
		return post
	};
	isLoading = () => {
		let loading;
		if(this.props.loading){
			loading = <div>loading...</div>
		}
		return loading
	};

	sendMessage = (toId) =>{
		console.log('sendMessage called' + toId)
	};

	deleteMessage = (id) =>{

	};

	componentDidMount(){
		//this.props.fetchPost();
		//this.props.fetchRelatedPost();
		this.sendMessage(1)

	}

	render(){
		return(
			<div>
			{this.isLoading()}
			{this.isPostFetched()}
			</div>
			)
	}

}
