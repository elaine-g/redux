import React,{ Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from '../Home/Home.jsx';
import Feed from '../Feed/Feed.jsx';
import NotFound from '../404/notfound.jsx';

import SearchComponent from '../Search/SearchComponent.jsx';
import HomeContainer from '../../containers/Home/HomeContainer.jsx';
import SearchContainer from '../../containers/search/SearchContainer.jsx';
import PostContainer from '../../containers/Post/PostContainer.jsx';
import TagContainer from '../../containers/Tags/TagContainer.jsx';
import Channel from '../Channel/Channel.jsx';
import Page from '../Page/Page.jsx'
import ReactGA from 'react-ga';
import ScrollToTop from '../ScrollToTop/ScrollToTop.jsx'

import logPageView from '../LogPageView/LogPageView'
/*
const logPageView = (props,store) => {
	console.log('updated ' + props.location.pathname);

	--
	// This should be part of your setup
	ReactGA.initialize('foo', { testMode: true,debug: true });
	// This would be in the component/js you are testing
	ReactGA.ga('send', 'pageview', window.location.pathname + window.location.search);
	// This would be how you check that the calls are made correctly
	--
	return null;
};
*/

class Middleware extends Component{
// first static pages,then dynamic.
	render(){
		return(
			
			<div>
			<Route component={logPageView} />
			<ScrollToTop>
			<Switch>
				
				<Route exact path='/' component={HomeContainer}  />
				<Route exact path='/search/' component={SearchComponent} />
				<Route exact path='/search/:query' component={SearchContainer} />
				<Route exact path='/tag/:tag' component={TagContainer} />
				<Route exact path='/:category/:year([0-9]{4})-:month([0-9]{2})-:slug' component={PostContainer} />
				<Route exact path='/:page' component={Page} />
				

				
				<Route path='*' component={NotFound} />
				
			</Switch>
			</ScrollToTop>
			</div>

			)
	}

}

export default Middleware;