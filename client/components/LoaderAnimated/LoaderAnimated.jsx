import React,{Component} from 'react'

const LoaderAnimated = (props) =>{
	if(!props.ready){
	return(
		<div>
			<span> loading ... </span>
		</div>
		)
	}else{
		return(<div>{ props.children }</div>)
	}
}
export default LoaderAnimated