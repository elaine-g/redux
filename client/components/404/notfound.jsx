import React,{Component} from 'react';
import { connect } from 'react-redux';
import HeaderActionCreators from '../../actions/HeaderActionCreators.jsx';
const mapStateToProps = (state) => {
	return{
		isDropDown : state.HeaderReducer.ui.dropdown.open 
	};
};


class NotFound extends Component{

	constructor(props){
		super(props);
	}

	componentWillUnmount(){
		if(this.props.isDropDown){
			console.log('aca en el unmount');
			this.props.dispatch(HeaderActionCreators.dropDown.topHeader(false));
		}
	}

	render(){
		return(
			<div>Not found 404</div>
			)
	}

}

export default connect(mapStateToProps)(NotFound);