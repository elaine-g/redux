import React,{Component} from 'react';
import { Link,withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';

import ArtistActionCreators from '../../actions/ArtistActionCreators';
import HeaderActionCreators from '../../actions/HeaderActionCreators';
import Navbar from '../Navbar/Navbar';
import Middleware from '../Middleware/Middleware';
import AdContainerTop from '../Ad/AdContainerTop'
class App extends Component{

	constructor(props){
		super(props);
	}

	componentDidMount(){
		let preloadCategories;
		let { dispatch } = this.props;
		if(typeof window !== "undefined"){
			preloadCategories = JSON.parse(document.getElementById('features').textContent).categories;
		}	
		dispatch(HeaderActionCreators.setLang());
		dispatch(HeaderActionCreators.setNavbarCategories(preloadCategories));	
				
	}

	render(){
		return(
			<div>
				<AdContainerTop />
				<Navbar/>
				<Middleware />
			</div>
			)
	}
}


export default withRouter(connect(null)(App));
