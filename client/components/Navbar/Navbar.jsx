// platforma de cambios,mercado libre pero de cambios.
/*               n1       n2
estas cambiando ____ por ____ (ui)
-> enviar request de cambios a todos los que cuenten con n2
--- interesdo- no me interesa- quizá=>match!
*/
import React,{ Component } from 'react';
import { connect } from 'react-redux';
import DropDown from '../DropDown/DropDown';
import MyItem from './MyItem/MyItem';
import { Link } from 'react-router-dom';

import NavbarStyles from './Navbar.css';
import classNames from 'classnames';

import { trackingClick } from '../../utils/Functions';
import HeaderActionCreators from '../../actions/HeaderActionCreators';


import ReactGA from 'react-ga';

class Navbar extends Component{

	constructor(props){
		super(props);
		this.handleScroll =  this.handleScroll.bind(this);	
		this.sendHeader =  this.sendHeader.bind(this);	
		this.openPopUp =  this.openPopUp.bind(this);	
		this.handleWindowClick =  this.handleWindowClick.bind(this);	
		this.trackingClick = trackingClick.bind(this);
	}

	sendHeader(data){
		let { dispatch } = this.props;
		dispatch(HeaderActionCreators.setHeader(data));
		if(data === false && this.props.ui.header.dropdown.open){
			this.openPopUp(false);
		}
	
	}

	openPopUp(value){
		let { dispatch } = this.props;
		dispatch(HeaderActionCreators.dropDown.topHeader(value));
	}

	handleScroll(event) {
	  var el = document.querySelector("[data-id='15']");
	  var pageY = window.scrollY;
	  if (el !== null && el.getBoundingClientRect().top <= 0) {
	  	if(!this.props.ui.header.sticky){
	  		if(this.props.ui.header.dropdown.open === true){
	  			this.openPopUp(false);
	  		}
	  		this.sendHeader(true);
	  	}
	  }else{
	  	if(this.props.ui.header.sticky){
	  		this.sendHeader(false);
	  	}
	  }
	}
	handleWindowClick(evt){
		//alert('cliclado!'); !document.getElementById('popup').contains(evt.target)
		//event.currentTarget.
		if(this.props.ui.header.dropdown.open && !document.getElementById('popup').contains(evt.target) && !document.getElementById('popupButton').contains(evt.target)){
			this.openPopUp(false);
		}
	}

	componentDidMount(){
		//let initialHeader = document.querySelector("[data-id='15']").getBoundingClientRect().top <= 0 ? true : false;
		//this.sendHeader(false);
		
		window.addEventListener('scroll', this.handleScroll);
		document.addEventListener('click', this.handleWindowClick);
	}
	/*
	Aún se me conceden cualidades de mi persona, excéntrica y pérdida de memoria.
	Aún el vértigo de surcar constantes y mares del cosmos 
	me pertenece. 
	Claridad,templanza.
	*/
	componentWillUnmount() {
		console.log('unmount...');
		window.removeEventListener('click',this.handleWindowClick);
		window.removeEventListener('scroll', this.handleScroll);
  	}

	render(){
		var classes = classNames({
			[NavbarStyles.navbar] : true,
			[NavbarStyles.fixed] : this.props.ui.header.sticky,
			[NavbarStyles.nav_relative] : !this.props.ui.header.sticky,
			[NavbarStyles.clearfix] :true
		});
		var logoClasses = classNames({
			[NavbarStyles.logo] : true
		});
		var headerClasses = classNames({
			[NavbarStyles.headerButton] : true
		});
		var ExploreButtonClasses = classNames({
			[NavbarStyles.button_explore] : true,
			[NavbarStyles.button_open] : this.props.ui.header.dropdown.open
		});
		if(this.props.header.categories){
			return(
				<div className={NavbarStyles.navbar_container}>
				<header className={classes} data-sticky={ this.props.ui.header.sticky }  id={NavbarStyles.navbar}>
					<div id={NavbarStyles.menu}>
						<div id={NavbarStyles.header_center} className={NavbarStyles.clearfix}>
							<div className={ NavbarStyles.brandHeader }>
								<a className={ExploreButtonClasses}  id="popupButton" href="javascript:void(0)" onClick={ () => this.openPopUp(!this.props.ui.header.dropdown.open)}> # Channels</a>
								<DropDown targetId={"popup"} display={this.props.ui.header.dropdown.open} items={this.props.header.categories} />
								<Link to={"/"} className={headerClasses}>

								<svg>
								<path className={logoClasses} d="M59.1 12c-2-1.9-4.4-2.4-6.2-2.4-4.4 0-7.3 2.6-7.3 8 0 3.5 1.8 7.8 7.3 7.8 1.4 0 3.7-.3 5.2-1.4v-3.5h-6.9v-6h13.3v12.1c-1.7 3.5-6.4 5.3-11.7 5.3-10.7 0-14.8-7.2-14.8-14.3 0-7.1 4.7-14.4 14.9-14.4 3.8 0 7.1.8 10.7 4.4L59.1 12zM68.2 31.2V4h7.6v27.2h-7.6zM88.3 23.8v7.3h-7.7V4h13.2c7.3 0 10.9 4.6 10.9 9.9 0 5.6-3.6 9.9-10.9 9.9h-5.5zm0-6.5h5.5c2.1 0 3.2-1.6 3.2-3.3 0-1.8-1.1-3.4-3.2-3.4h-5.5v6.7zM125 31.2V20.9h-9.8v10.3h-7.7V4h7.7v10.3h9.8V4h7.6v27.2H125zM149.2 13.3l5.9-9.3h8.7v.3l-10.8 16v10.8h-7.7V20.3L135 4.3V4h8.7l5.5 9.3z">
								</path>
								</svg>
								</Link>
							</div>
							<div className={NavbarStyles.nav_right}>
								<ul>
								<li><Link to={"/about"} onClick={() => this.trackingClick()} >About</Link></li>
								<li><Link to={"/creators"} onClick={() => this.trackingClick()} >Creators!</Link></li>
								<li><Link to={"/jobs"} onClick={() => this.trackingClick()} >Jobs</Link></li>
								<li><Link to={"/evy"} onClick={() => this.trackingClick()} >Evy</Link></li>
								<li><Link to={"/music/hard-times-paramore"} onClick={() => this.trackingClick()} >Hard Times</Link></li>
								<li><Link to={"/home-container"} onClick={() => this.trackingClick()} >Featured posts!</Link></li>
								<li><Link to={"/music/2018-02-fornine"}  >forninte posts!</Link></li>
								<li><Link to={"/music/2012-02-paramore-cruise3"}  >paramore posts!</Link></li>
								<li><Link to={"/tag/loquesea"}  >loquesea posts!</Link></li>
								<li><Link to={"/tag/parawhore"}  >parawhore posts!</Link></li>


								</ul>
							</div>
						</div>
					</div>
				</header>
				</div>
				)
		}else{
			return(
				<p>Loading</p>
				)
		}
	}

}

const mapStateToProps = (state, props) => {
	return {
	    header: {
	    	categories: state.HeaderReducer.data.categories
	    },
	    ui: {
	    	header: {
	    		sticky: state.HeaderReducer.ui.sticky,
		    	dropdown:{
		    		open: state.HeaderReducer.ui.dropdown.open,
		    		last_action: state.HeaderReducer.ui.dropdown.last_action
		    	}
	    	},
	    },
	};
}




export default connect(mapStateToProps)(Navbar);