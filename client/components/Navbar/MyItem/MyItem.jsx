import React,{ Component } from 'react';
import { Link } from 'react-router-dom';

class MyItem extends Component{

	constructor(props) {
	  super(props);
	 
	}

	componentDidMount(){
		if(!this.props.data){
			// do anything no data!
		}
	}

	render(){
		const markup = <li key={this.props.index}><Link data-category-title={ this.props.data.name } to={this.props.data.link}>{this.props.data.name}</Link></li>;
		if(this.props.data){
			return(
				markup
				)
		}else{
			return <p>No data</p>;
		}
	}


}
export default MyItem;