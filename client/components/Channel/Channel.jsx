import React,{Component} from 'react'

import { connect } from 'react-redux'

import HeaderActionCreators from '../../actions/HeaderActionCreators'
import ChannelActionCreators from '../../actions/ChannelActionCreators'

class Channel extends Component{

	constructor(props) {

	  super(props);
	  
	}

	componentDidMount(){
		const { dispatch } = this.props;
		dispatch(ChannelActionCreators.setCurrentChannel(this.props.currentChannel));
		if(this.props.ui.header.sticky){
			dispatch(HeaderActionCreators.setHeader(false));
		}
	}

	componentWillUnmount(){
		const { dispatch } = this.props;
		dispatch(ChannelActionCreators.resetChannel());
	}

	componentWillReceiveProps(nextProps){
		const { dispatch } = this.props;
		if(this.props.currentChannel !== nextProps.match.params.channel){
			if(this.props.ui.header.dropDown.open){
				dispatch(HeaderActionCreators.dropDown.topHeader(false));
			}
			dispatch(ChannelActionCreators.changeChannel(nextProps.match.params.channel));
		}
	}


	render(){
		if(this.props.channel.current === 'geek'){
			return(
				<div>
				{ this.props.currentChannel }
				a<br />
				a<br />
				a<br />
				a<br />
				a<br />
				a<br />
				<img data-id="15" src="https://media.giphy.com/media/xGnDe5cN8UgHS/giphy.gif" />
				</div>
				)
		}
		return(
			<div>
			{ this.props.currentChannel }
			<p>ó</p>
			{ this.props.channel.current }
			</div>
			)
	}

}
function mapStateToProps(state,props){
	return{
		currentChannel: props.match.params.channel,
		ui:{
			header:{
				sticky: state.HeaderReducer.ui.sticky,
				dropDown: { open: state.HeaderReducer.ui.dropdown.open}
			}
		},
		channel: {
			current: state.ChannelReducer.current
		}
	};
}
export default connect(mapStateToProps)(Channel);
