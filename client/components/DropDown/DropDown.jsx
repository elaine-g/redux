import React,{ Component } from 'react';
import MyItem from '../Navbar/MyItem/MyItem.jsx';
import styles from './../Navbar/Navbar.css';

class DropDown extends Component{

	constructor(props) {
	  super(props);
	
	}

	render(){
		return(
			<div id={this.props.targetId} className={styles.popup} style={{ display: this.props.display === true ? 'block' : 'none'}}>
			{this.props.items &&
				this.props.items.map((value,index)=>{
					return(
						<MyItem index={index} data={value} />
						)
				})
			}				
			</div>
			)
	}

}

export default DropDown;