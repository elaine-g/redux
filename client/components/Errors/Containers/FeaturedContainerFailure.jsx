import React,{ Component } from 'react'

class FeaturedContainerFailure extends Component{

	constructor(props) {
		super(props);
	  	this.state = {
	  		component : null
	  	};
	}

	setComponentError = () => {
		let component = null;
		switch(this.props.code){
			case 404:
				component = <div> Data not found </div>
			break;
			default:
				if(this.props.errorMessage)
					component = <div>{this.props.errorMessage} <button onClick={ () => this.handleRetry() }> retry </button></div>
				else
					component = <div>unknows <button onClick={ () => this.handleRetry() }> retry </button></div>
			break;
		}
		return component
	};

	handleRetry = () => {
		this.props.retry();
	};

	componentDidMount(){
		this.setState({ component: this.setComponentError() });
	}

	componentWillUnmount(){
		console.log('unmount featured')
	}

	render(){
		return(this.state.component)
	}

}

export default FeaturedContainerFailure