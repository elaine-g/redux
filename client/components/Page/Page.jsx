import React,{ Component } from 'react'
import { connect } from 'react-redux'
import NotFound from '../404/notfound.jsx';
import PageActionCreators from '../../actions/PageActionCreators';
import ChannelActionCreators from '../../actions/ChannelActionCreators';
import HeaderActionCreators from '../../actions/HeaderActionCreators'
import PAGE_CONSTANTS from '../../constants/PageConstants/PageConstants';
class Page extends Component{

	constructor(props){
		super(props);
	}
	/*
	pageExists(page){
		var pages = ['creators','jobs'];
		if(pages.indexOf(page) !== -1){
			this.setState({
				isPage: true
			});
			return true;
		}
		return false;
	}

	channelExists(channel){

	}

	authorExists(author){
		var authors = ['elaine','evy'];
		if(authors.indexOf(author) !== -1){
			this.setState({
				isAuthor: true
			});
			return true;
		}
		return false;
	}
	*/
	componentDidMount(){
		//this.props.detectType(this.props.currentController);
		this.props.detectAll(this.props.currentController);
	}
	componentDidUpdate(prevProps,prevState){
	}
	componentWillUnmount(){
		this.props.reset();
	}
	componentWillReceiveProps(nextProps){
		/*
		if(nextProps.match.params.page !== this.props.currentController){
			console.log('cambio,hacer update')

			this.props.reset();

			this.props.setIsApage(nextProps.match.params.page);
			this.props.setIsAchannel(nextProps.match.params.page);
	
		if(nextProps.isPage && this.props.isPage){
			console.log('page++')
		}else if(nextProps.isChannel && this.props.isChannel){
			console.log('channel++')
		}
		}
		*/
		/*
		if(this.props.isPage !== nextProps.isPage && nextProps.isChannel){
			console.log(' === is channel === ');

		}
		if(this.props.isChannel !== nextProps.isChannel && nextProps.isPage){
			console.log(' === is page === ');
		}
		if(!nextProps.isFetching && this.props.isPage !== nextProps.isPage && nextProps.isChannel){
			console.log(' ****** is channel ****** ');
		}
		if(!nextProps.isFetching && this.props.isChannel !== nextProps.isChannel && nextProps.isPage){
			console.log(' ****** is page ****** ');
		}
		*/

		if(nextProps.match.params.page !== this.props.currentController){
			nextProps.detectAll(nextProps.match.params.page);
			if(this.props.ui.header.dropDown.open){
				this.props.dispatch(HeaderActionCreators.dropDown.topHeader(false));
			}
		}
	}
	render(){
		if(this.props.isFetching){
			return(
			<div>cargando wn...</div>)
		}
		if(this.props.exists && this.props.isPage){
			return(
			<div dangerouslySetInnerHTML={{__html: this.props.data.html }}>
			</div>)
		}else if(this.props.exists && this.props.isChannel){
			return(
			<div dangerouslySetInnerHTML={{__html: this.props.data.html }}>
			</div>)
		}else if(this.props.exists && this.props.isAuthor){
			return(
			<div dangerouslySetInnerHTML={{__html: this.props.data.html }}>
			</div>)
		}else{
			return(
				<NotFound />
				)			
		}
	}

}

const mapStateToProps = (state,props) => {
	return{
		exists    : state.PageReducer.exists,
		isPage    : state.PageReducer.type.isPage,
		isChannel : state.PageReducer.type.isChannel,
		isAuthor  : state.PageReducer.type.isAuthor,
		isFetching : state.PageReducer.fetching,
		data	  : state.PageReducer.data,
		ui: {
			header:{
				dropDown:{
					open: state.HeaderReducer.ui.dropdown.open
				}
			}
		},
		currentController: props.match.params.page
	}
}

const mapDispatchToProps = (dispatch) =>{
	return{
		detectType: (page) => {
			dispatch(PageActionCreators.setIsApage(page),dispatch),
			dispatch(PageActionCreators.setIsAchannel(page),dispatch),
			dispatch(PageActionCreators.setIsAauthor(page),dispatch),
			dispatch
		},
		channel:{
			change: (channel) => dispatch(ChannelActionCreators.changeChannel(channel))
		},
		detectAll: (page) => dispatch(PageActionCreators.whatFuckingIs(page)),
		reset: () => dispatch(PageActionCreators.reset()),
		dispatch
		}

}

export default connect(mapStateToProps,mapDispatchToProps)(Page)