import React,{Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import FormattedMessage from '../../utils/FormattedMessage';
import HomeContainer from './HomeContainer';
import ReactPlaceholder from 'react-placeholder';
import "../../../node_modules/react-placeholder/lib/reactPlaceholder.css";

import HeaderActionCreators from '../../actions/HeaderActionCreators';
import {API} from '../../utils/Api.jsx';

class Home extends Component{

	constructor(props){
		super(props);
	}

	componentDidMount(){	
		if(this.props.last_name === null){
			this.props.getName();
		}
	}
	componentWillUnmount(){
		if(this.props.isDropDown){
			this.props.dispatch(HeaderActionCreators.dropDown.topHeader(false));
		}
		console.log('desmontando.. l');
	}
	render(){
		return(
			<ReactPlaceholder ready={this.props.isReady || this.props.error !== false} delay={5000} showLoadingAnimation={true} rows={7} type='media'>
  			<HomeContainer/>
			</ReactPlaceholder>
			)
	}
}


function mapStateToProps(state,props) {
  return {
    error: state.ArtistReducer.error,
    last_name: state.ArtistReducer.data.last_name,
    header:state.ArtistReducer.data.header,
    isDropDown: state.HeaderReducer.ui.dropdown.open,
    isReady : state.ArtistReducer.data.ready
  };
}
function mapDispatchToProps(dispatch,props){
	return{
		getName : () => dispatch(API.API_GET_NEWS(),dispatch),
		dispatch
	}
}
export default connect(mapStateToProps,mapDispatchToProps)(Home);
