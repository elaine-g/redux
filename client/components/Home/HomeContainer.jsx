// crear un api similator donde puedas crear apis simuladas /api/102/custom-end-point -> custom response.upload data.json can set response or time out can set allowed headers can set method. can set custom code response. can set pretiffy can watch custom libraries response . login or anon. save in session apis. free account -> can set 1 user, premium up to 2 users (can share library response)

import React,{Component} from 'react';
import { Switch, Route,Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import ArtistActionCreators from '../../actions/ArtistActionCreators.jsx';
import PostActionCreators from '../../actions/PostActionCreators';
import FormattedMessage from '../../utils/FormattedMessage.jsx';
import NavbarStyles from '../Navbar/Navbar.css';
class HomeContainer extends Component{

	constructor(props){
		super(props);
		this.retry = this.retry.bind(this);
	}

	retry(){
		this.props.actions.retry();
	}
	componentWillUnmount() {

  	}

  	componentDidMount(){
  		this.props.fetchPostsFake();
  	}
	render(){

		var style = {
			color:'blue'
		};
		//const markup = <div> { this.props.last_name } <button onClick={ this.change_name }>¿Another name?</button>
		//</div>;
		// <button onClick={ this.retry }>Wanna retry data?</button>
		if(this.props.posts.error === true){
			return(
				<div>
					{ this.props.posts.errorMessage }
				</div>
				)
		}
		const markupError = <div id={NavbarStyles.wrapper}>
		<div>sticky <b>{this.props.header}</b>
		</div>a<br/>a<br/>
		a<br/>
		a<br/>
		a<br/>
		a<br/>
		a<br/>
		<p>b</p>
		<div>{
			this.props.posts && this.props.posts.data.map((post,index) => {
				return(
					<div key={ index }>
					<b> { post.id } </b>
					<p>{ post.first_name }</p>
					<Link to={"/music/hard-times-de-paramore"}><img height="520" width="520" src={post.avatar} /></Link>
					</div>
					)
			})
		}</div>
		<br/>
		a<br/>
		a<br/>
		a<br/>
		a<br/>
		a<br/>
		a<br/><br/>
		a<br/>a<br/>a
		<img id="post" data-id="15" src="https://i.pinimg.com/originals/bd/20/ac/bd20ac3cccc3355720175b2bad03e57a.jpg"/><br/>
		
		<img id="post" data-id="5" src="https://media.giphy.com/media/xT9IglN8LGrTTBPKG4/200_s.gif"/><br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>a<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/>b<br/><b> { this.props.error }  </b></div>;
		const Hayley = <div><video id="gif-mp4" poster="https://media.giphy.com/media/xT9IglN8LGrTTBPKG4/200_s.gif" width="480" height="286" autoplay="" loop="true">
            <source src="https://media.giphy.com/media/xT9IglN8LGrTTBPKG4/giphy.mp4" type="video/mp4" />
            Your browser does not support the mp4 video codec.
        </video></div>;
        if(this.props.error !== false && this.props.error === 'Network Error'){
        	return(
        		<div>
        		{ this.props.error } <a href="#" onClick={ () => this.retry() }> Wanna retry? </a>
        		</div>
        		)
        }
    	return(
    		<div>
    		{
    			markupError
    		}
    		
    		<FormattedMessage message={this.props.last_name}/>
    		</div>
    	)    
	} 	

}

function retry(){
	return function(dispatch,getState){
		return axios.get('https://reqres.in/api/users/1').then((res)=>{
			dispatch(ArtistActionCreators.getName(res.data.data.first_name));
			dispatch(ArtistActionCreators.setError(false));
		}).catch((error)=>{
			if(error.response){
		    	dispatch(ArtistActionCreators.setError(error.response.status));
		    }else if(error.request){
		    	dispatch(ArtistActionCreators.setError(error.message));
		    }
		});			
	};
}

/*
function makeASandwichWithSecretSauce() {

  // Invert control!
  // Return a function that accepts `dispatch` so we can dispatch later.
  // Thunk middleware knows how to turn thunk async actions into actions.
  return function (dispatch,getState) {
  	// if(!getState().last_name)
		const url = 'https://reqres.in/api/unknown/23';
		const request = axios.get(url);
	    return request.then((res)=>{
	    	dispatch(ArtistActionCreators.getName(res.data.data.first_name));
	    }).catch((error)=>{
	    	dispatch(ArtistActionCreators.setError(error.response.status));
	    });
    
  };
}
*/
function mapStateToProps(state, props) {
  // armamos un objeto solo con los
  // datos del store que nos interesan
  // y lo devolvemos
  /*
  return {
    last_name: state.last_name,
    error: state.error,
    header: state.header
  };
  */
  return{
  	last_name: state.ArtistReducer.data.last_name,
  	error: state.ArtistReducer.error,
  	posts : state.PostReducer.posts
  };
}

const mapDispatchToProps = (dispatch) => {
	return {
  		fetchPosts: (id) => dispatch(PostActionCreators.fetchPosts(id), dispatch),
  		fetchPostsFake: () => dispatch(PostActionCreators.fetchPostsFake(),dispatch),
  		dispatch
  	}
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeContainer);
