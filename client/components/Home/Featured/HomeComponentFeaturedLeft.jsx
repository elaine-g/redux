import React,{ Component } from 'react'
import HomePostItem from './HomePostItem'
import postFeaturedStyles from '../../../styles/postFeaturedStyles.css';
export default class HomeComponentFeaturedLeft extends Component{

	constructor(props) {
	  super(props);
	  this.state = { };
	}	

	componentDidMount(){}

	showComponent = () => {
		if(this.props.posts !== null){
			return this.props.posts && this.props.posts.map((item,index) => {
				return(
					<HomePostItem {...item}  />
					)
			})
		}
	};

	render(){
		return(
			<div className={ postFeaturedStyles.leftContainer }>
			{ this.showComponent() }
			</div>
			)
	}

}
