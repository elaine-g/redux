import React,{ Component } from 'react'
import postFeaturedStyles from '../../../styles/postFeaturedStyles.css';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
export default class HomePostItem extends Component{
	
	constructor(props) {
	  super(props);
	  
	}

	render(){
		const {index,id,title,description,url} = this.props;
		const thumbnail = this.props.cover.thumbnail;
		if(title){
			return(	
				<div key={index} className={ postFeaturedStyles.featured_container }>
				<span>{ title }</span>
				<Link to={url}><img className={ postFeaturedStyles.featured_item_thumbnail } src={ thumbnail } /></Link>
				</div>
				)
		}
	}

}
