import React,{ Component } from 'react'
import HomePostItem from './HomePostItem'
import postFeaturedStyles from '../../../styles/postFeaturedStyles.css';

export default class HomeComponentFeaturedRight extends Component{

	constructor(props) {
	  super(props);
	  this.state = {};
	}

	showComponent = () => {
		if(this.props.posts !== null){
			return this.props.posts && this.props.posts.map((item,index) => {
				return(
					<HomePostItem {...item}  />
					)
			})
		}
	};

	render(){
		return(
			<div className={ postFeaturedStyles.rightContainer }>
			{ this.showComponent() }
			</div>
			)
	}

}