import React,{ Component } from 'react'

class ScrollToTop extends Component {

	constructor(props) {
	  super(props);
	}



	componentDidUpdate(prevProps) {
		setTimeout(() => {
	    if (this.props.location !== prevProps.location) {
	      window.scrollTo(0, 0);
	    }
	    /*
	    if(this.props.history.action === "POP"){
	    	console.log("POP!");
	    	return;
	    }
	    */
		});
	  }

	  render() {
	    return this.props.children
	  }

}

export default ScrollToTop
