const PAGE_CONSTANTS = {
	NOT_EXISTS : 'NOT_EXISTS',
	IS_PAGE: 'IS_PAGE',
	IS_NOT_PAGE: 'IS_NOT_PAGE',
	IS_CHANNEL: 'IS_CHANNEL',
	IS_AUTHOR: 'IS_AUTHOR',
	RESET: 'RESET',
	NOT_FOUND: 'NOT_FOUND',
	REQUEST_PAGE: 'REQUEST_PAGE',
	REQUEST_PAGE_FINISHED: 'REQUEST_PAGE_FINISHED'

}
export default PAGE_CONSTANTS