export default {
	CREATE_ACCOUNT : 'create account',
	DEPOSIT_INTO_ACCOUNT : 'deposit into account',
	WITHDRAW_FROM_ACCOUNT : 'withdraw from account',
	RESET_ACCOUNT: 'reset account'
}


export const ArtistConstant = {
	SHOW_INFORMATION : 'show information',
	CHANGE_AGE : 'change age',
	GET_NAME : 'get name',
	ERROR_GET_NAME: 'error get name',
	CHANGE_NAME_GEORGE: 'change name george',
	SET_HEADER: 'set header',
	GET_NAVBAR: 'get navbar'
}

export const REQUEST_POSTS = 'request posts';
export const RECEIVE_POSTS = 'receive posts';
export const ERROR_RECEIVE_POSTS = 'error receive posts';

export const SET_DROPDOWN = 'set window header open';
