const analyticsMiddleware = ({ dispatch, getState }) => next => action => {

	switch(action.type){
		
		case 'set current channel':
			console.log('analyticsMiddleware triggered:'+ action.type);
		break;

		case 'change channel':
			console.log('analyticsMiddleware triggered:'+ action.type);
		break;

		case 'reset channel':
			console.log('reset!');
		break;

	}
    return next(action);

}

export default analyticsMiddleware;