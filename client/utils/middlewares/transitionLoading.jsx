import CHANNEL_CONSTANTS from '../../constants/ChannelConstants/ChannelConstants.jsx'
const transitionLoading = (store) =>{
	return function(next){
		return function(action){
			console.log('transition Middleware activated!');
			
			switch(action.type){
				case CHANNEL_CONSTANTS.CHANGE_CHANNEL:
				console.log('change middleware intercepted before!')
				break;
			}
			return next(action);
		}
	}
}
export default transitionLoading