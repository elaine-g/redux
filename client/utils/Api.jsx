import axios from 'axios';
import ArtistActionCreators from '../actions/ArtistActionCreators';

export const API = {
 	API_GET_NEWS(){
		return function(dispatch,getState){
			
			return axios.get('https://reqres.in/api/users/1').then((res)=>{
				 setTimeout(function () {
	        		dispatch(ArtistActionCreators.getName(res.data.data.first_name,true));
	      		  }, 2000)
				
			}).catch((error)=>{
				if(error.response){
			    	dispatch(ArtistActionCreators.setError(error.response.status));
			    }else if(error.request){
			    	setTimeout(function () {
	        			return axios.get('https://reqres.in/api/users/1').then((res) =>{
	        				dispatch(ArtistActionCreators.getName(res.data.data.first_name,true));
	        			}).catch((error) =>{
	        				dispatch(ArtistActionCreators.setError(error.message));
	        			});
	      		  }, 5000);
			    }else{
			    	// do anything
			    }
			});			
		};
	}
}