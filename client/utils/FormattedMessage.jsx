import React,{ Component } from 'react';

/*
const FormattedMessage = (props) => (
	<div dangerouslySetInnerHTML={{__html: props.message }}>
	</div>
	);
*/

class FormattedMessage extends Component{

	constructor(props){
		super(props);
	}

	render(){
		if(this.props.children){
			return(
			<div dangerouslySetInnerHTML={{__html: this.props.children }}>
			</div>
				)
			}
		if(this.props.id !== 'undefined' && this.props.message !== 'undefined'){
			return(
				<div dangerouslySetInnerHTML={{__html: this.props.message }}>
				</div>			
				)
		}else{
			return new Error('there are once error.');
		}
	}

}

export default FormattedMessage;