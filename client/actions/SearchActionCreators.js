import SEARCH_CONSTANTS from '../constants/SearchConstants/SearchConstants'
import SearchAPI from '../api/SearchAPI'

let SearchActionCreators = {
	searchQuery(query,filters){
		return(dispatch,getState) =>{
			dispatch({ type: SEARCH_CONSTANTS.REQUEST_SEARCH_KEYWORD, keyword: query });
			SearchAPI.get(query,filters).then((response) => {
				if(response.status){
					return dispatch({
						type: SEARCH_CONSTANTS.RECEIVE_SEARCH,
						result: response.data
					})
				}else{
					return dispatch({ type: SEARCH_CONSTANTS.REQUEST_SEARCH_KEYWORD_FAIL, message: response.message })
				}
			}).catch((err) =>{
				// do anything...
				dispatch({ type: SEARCH_CONSTANTS.REQUEST_SEARCH_KEYWORD_FAIL, message: err.message })
			})	
		}
	}
};

export default SearchActionCreators;