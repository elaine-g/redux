import PostAPI from '../api/PostAPI'
import POST_CONSTANTS from '../constants/PostConstants/PostConstants'
import POST_RELATED_CONSTANTS from '../constants/PostConstants/PostRelatedConstants'

// Action Creators plain objects.

const requestPost = (post) => ({ type: POST_CONSTANTS.REQUEST_POSTS, payload: post.slug })
const requestPostFailure = (error) => ({ type: POST_CONSTANTS.ERROR_RECEIVE_POSTS, payload: error })
const requestPostFailureWithCode = (error) => ({ type: POST_CONSTANTS.ERROR_RECEIVE_POSTS, payload: error  })
export const reset = () => ({ type: POST_CONSTANTS.RESET_POSTS })

const requestRelated = () => ({ type: POST_RELATED_CONSTANTS.REQUEST_RELATED_POSTS })

// Action Creators async function (redux-thunk)

let PostActionCreators = {
	fetchPosts(id){
		return (dispatch,getState) =>{
			PostAPI.fetchPostById(id).then((res) =>{
				if(res.data){
					dispatch({ type: POST_CONSTANTS.RECEIVE_POSTS, article: res.data });
				}
			});
		};
	},
	fetchPostsFake(){
		return (dispatch,getState) =>{
			dispatch(requestPost());
			let posts = PostAPI.fetchPostsFake();
			let error = false;
			if(posts.data && !error){
				dispatch({
					type : POST_CONSTANTS.RECEIVE_POSTS,
					article : posts.data 
				})
			}else{
				dispatch({ type: POST_CONSTANTS.ERROR_RECEIVE_POSTS, error: 'Error in request posts.'})
			}
		}
	},
	fetchPost(url){
		return(dispatch,getState) => {
			dispatch(requestPost(url));
			PostAPI.fetchPost(url).then((res) => {
				dispatch({
					type: POST_CONSTANTS.RECEIVE_POSTS,
					payload: res
				})
			}).catch((err) => {
				if(err.response){
					dispatch(requestPostFailureWithCode({ error: { status: true, message: err.message,code: err.response.status } }))
				}else{
					dispatch(requestPostFailure({ error: { status: true, message: err.message } }))
				}
			});
		}
	},
	fetchRelatedPost(){
		return(dispatch,getState) => {
			dispatch(requestRelated())

		}
	}
};

export default PostActionCreators;