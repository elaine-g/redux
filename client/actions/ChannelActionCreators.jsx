import CHANNEL_CONSTANTS from '../constants/ChannelConstants/ChannelConstants'

let ChannelActionCreators = {
	setCurrentChannel(channel){
		return { type: CHANNEL_CONSTANTS.SET_CURRENT_CHANNEL , channel: channel  }
	},
	changeChannel(channel){
		return { type: CHANNEL_CONSTANTS.CHANGE_CHANNEL , channel: channel }
	},
	resetChannel(){
		return { type: CHANNEL_CONSTANTS.RESET_CHANNEL }
	}
}

export default ChannelActionCreators
