import HomeAPI from '../api/HomeAPI'
import HOME_CONSTANTS from '../constants/HomeConstants/HomeConstants'

const request = () => ({ type: HOME_CONSTANTS.FETCH_FEATURED_POSTS_REQUEST });
const receiveFeaturedPosts = (featured,time) => ({ type: HOME_CONSTANTS.FETCH_FEATURED_POSTS_SUCCESS , payload : { featured : featured, time : time }  })
const errorReceiveFeatured = (err) => ({ type: HOME_CONSTANTS.FETCH_FEATURED_POSTS_FAILURE , error: { message: err.message } })
const errorReceiveFeaturedWithCode = (err) => ({ type: HOME_CONSTANTS.FETCH_FEATURED_POSTS_FAILURE , error: { message: err.message ,code: err.response.status} })
const HomeActionCreators = {

	fetchFeatured(){
		return (dispatch,getState) => {
			dispatch(request());
			HomeAPI.fetchFeatured().then((res) => {
				if(res.status){
					dispatch(receiveFeaturedPosts(res.featured,res.time + (60 * 5)))
				}
			}).catch((err) => {
				if(err.response){
					dispatch(errorReceiveFeaturedWithCode(err))
				}else{
					dispatch(errorReceiveFeatured(err));
				}
			})
		}
	}

};
export default HomeActionCreators;