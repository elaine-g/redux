import PAGE_CONSTANTS from '../constants/PageConstants/PageConstants'
import CHANNEL_CONSTANTS from '../constants/ChannelConstants/ChannelConstants'
import PageAPI from '../api/PageAPI.js'
import PostAPI from '../api/PostAPI.js'
import axios from 'axios'
let PageActionCreators = {

	setIsApage(page){
		return (dispatch,getState) =>{
			var fakePage = PageAPI.getFakePage(page);
			if(fakePage !== false){
				dispatch({ type: PAGE_CONSTANTS.IS_PAGE, exists: true, data: fakePage })
			}
			return false;
		}
	},

	setIsAchannel(channel){
		return (dispatch,getState) =>{
			let fakeChannel;
			fakeChannel = PageAPI.getFakeChannel(channel);
			if(fakeChannel !== false){
				dispatch({ type: PAGE_CONSTANTS.IS_CHANNEL, exists: true, data: fakeChannel })
			//	dispatch({ type: CHANNEL_CONSTANTS.SET_CURRENT_CHANNEL, channel: channel })
			}
			return false;
		}		
	},

	setIsAauthor(author){
		return (dispatch,getState) =>{
			let fakeAuthor;
			fakeAuthor = PageAPI.getFakeAuthor(author);
			if(fakeAuthor !== false){
				dispatch({ type: PAGE_CONSTANTS.IS_AUTHOR, exists: true, data: fakeAuthor })
			//	dispatch({ type: CHANNEL_CONSTANTS.SET_CURRENT_CHANNEL, channel: channel })
			}
			return false;
		}			
	},
	/*
	whatFuckingIs(page){
		return(dispatch,getState) =>{
			let isPage = PostAPI.fetchPostById(1);
			let isChannel = PostAPI.fetchPostById(2);
			let isAuthor = PostAPI.fetchPostById(3);

			axios.all([isPage,isChannel,isAuthor]).then(axios.spread(function(isp,isc,isa){
				console.log('/// axios response///')
				console.log(isp.page)
				console.log(isc.page)
				console.log(isa.page)
			}));
		}
	},
	*/

	whatFuckingIs(page){
		return(dispatch,getState) => {
			dispatch({ type: PAGE_CONSTANTS.REQUEST_PAGE })
			let response = PostAPI.fetchWhatFuckingIs(page)
			if(response.status === true){
				switch(response.type){
					case 'author':
						console.log('////// author ');
						dispatch({ type: PAGE_CONSTANTS.IS_AUTHOR, exists: true, data: response.data })
					break;
					case 'page':
						console.log('////// page ');
						dispatch({ type: PAGE_CONSTANTS.IS_PAGE, exists: true, data: response.data })
					break;
					case 'category':
						console.log('////// category ');
						dispatch({ type: PAGE_CONSTANTS.IS_CHANNEL, exists: true, data: response.data })
					break;

					default:
						console.log('////// not found ');
						dispatch({ type: PAGE_CONSTANTS.RESET });
					break;
				}
				//setTimeout(() => {
				//dispatch({ type: PAGE_CONSTANTS.REQUEST_PAGE_FINISHED})
				//},2000)
				dispatch({ type: PAGE_CONSTANTS.REQUEST_PAGE_FINISHED})
			}else{
				console.log('////// not found 2');
				dispatch({ type: PAGE_CONSTANTS.NOT_FOUND });
				dispatch({ type: PAGE_CONSTANTS.REQUEST_PAGE_FINISHED })
			}
		}
	},

	reset(){
		return { type: PAGE_CONSTANTS.RESET };
	}
};

export default PageActionCreators;