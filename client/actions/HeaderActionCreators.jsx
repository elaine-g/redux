import HEADER_CONSTANT from '../constants/HeaderConstants/HeaderConstants.jsx';
export const requestLang = () => ({ type: 'REQUEST_LANG' })
let HeaderActionCreators = {
	setNavbarCategories(categories){
		return { type: HEADER_CONSTANT.SET_NAVBAR_CATEGORIES, categories: categories};
	},
	setHeader(status){
		return { type: HEADER_CONSTANT.SET_HEADER_STICKY, status: status};
	},
	dropDown : {
		topHeader(status){
			return {
				type: HEADER_CONSTANT.SET_DROPDOWN_HEADER,
				open: status,
				last_action:!status
			};
		}
	},
	setLang(){
		return(dispatch,getState) => {
			dispatch(requestLang())
		}
	}
};

export default HeaderActionCreators;