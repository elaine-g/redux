import axios from 'axios'
import TAG_CONSTANTS from '../constants/TagConstants/TagConstants'
import { fetchTag } from '../api/TagAPI'

const requestTag = (tag) => ({ type: TAG_CONSTANTS.FETCH_TAG_REQUEST, payload: tag })
const receiveTag = (result) => ({ type: TAG_CONSTANTS.FETCH_TAG_SUCCESS, payload: result })
const errorReceiveTag = (err) => ({ type: TAG_CONSTANTS.FETCH_TAG_FAILURE, payload: err })

let TagActionCreators = {
	
	fetchTag(tag){
		return(dispatch,getState) =>{
			dispatch(requestTag(tag))
			fetchTag(tag).then((res) => {
				dispatch(receiveTag(res))
			}).catch((err) => {
				dispatch(errorReceiveTag(err))
			})
		}
	}
}

export default TagActionCreators