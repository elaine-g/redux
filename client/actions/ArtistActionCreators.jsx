import {ArtistConstant as Constants} from '../constants/Constants.jsx';

const ArtistActionCreators = {

	showInformation(show){
		return{
			type: Constants.SHOW_INFORMATION,
			img: 'https://i.pinimg.com/564x/6e/9a/5f/6e9a5f0f06879fddce47b11a143c3686.jpg',
			name: 'Hayley Rica williams',
			job: 'Cantante',
			age: '29',
			isShow: show
		};
	},
	changeAge(age){
		return{
			type: Constants.CHANGE_AGE,
			age: age
		};
	},

	getName(result,ready){
		return{
			type: Constants.GET_NAME,
			payload: result,
			ready: ready
		};
	},

	setError(error){
		return{
			type: Constants.ERROR_GET_NAME,
			error: error
		}
	},

	changeName(name){
		return{
			type: Constants.CHANGE_NAME_GEORGE,
			name: name
		}
	},
	setHeader(header){
		return{
			type: Constants.SET_HEADER,
			header: header
		}
	}

};
export default ArtistActionCreators;