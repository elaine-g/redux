import Constants from '../constants/Constants.jsx';


const IVA = (amount,iva) =>{
	const newiva = amount * iva / 100;
	let total = amount + newiva;
	return total;
};

// Action Creators
/*
Pueden ser definidas también como

const ADD_TODO = {
	type: ADD_TODO,
	text: 'Build my first Redux App'
};
import { ADD_TODO, REMOVE_TODO } from '../actionTypes'
function addTodo(text) {
  return {
    type: ADD_TODO,
    text
  }
}

*/
const BankActionCreators = {
	depositIntoAccount(amount){
		return{
			type: Constants.DEPOSIT_INTO_ACCOUNT,
			amount: amount,
			modified:true
		};
	},
	withdrawAccount(amount){
		return{
			type: Constants.WITHDRAW_FROM_ACCOUNT,
			amount: IVA(amount,16),
			modified:true
		};
	},
	resetAccount(){
		return{
			type: Constants.RESET_ACCOUNT,
			amount: 0,
			modified:false
		};
	}
};
export default BankActionCreators;