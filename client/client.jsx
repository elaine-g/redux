import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App.jsx';

import store from './store/store.jsx';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>,
	document.getElementById('root'));