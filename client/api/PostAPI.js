import axios from 'axios'

const API_BASE_URL = 'http://localhost:3000/';

let PostAPI = {
	
	fetchPostById(id){
		return axios.get('https://reqres.in/api/users?page='+id).then((response) => {
			if(response.data){
				return response.data;
			}
		}).catch((error) =>{
			return { error:true,message : error.message };
		})
	},
	fetchPostsFake(){
		return { request: '/api/data', data: [
		{
			id: 1,
			last_name: 'Evy Rosas',
			avatar: 'https://scontent.fmex2-1.fna.fbcdn.net/v/t1.0-9/18622386_681682055376134_5086833918968659100_n.jpg?_nc_eui2=v1%3AAeG7r-xEykNk1DjtNY7fRi-jfNXGBLQ_BdWQX173PGWcUsMo-8OI9W_6VDKXwWhltMcdbxH074eXnyBXehh93vgwmtkKSo3511WVzL1cbfvWYQ&oh=db95a9e5a6271bd349d3088a44cbdd91&oe=5B311F80'
		},{
			id: 2,
			last_name: 'Hayley Williams',
			avatar: 'https://i.pinimg.com/236x/45/b2/cc/45b2cceb6ff6c2f3334d3c412add803d.jpg'

		},{
			id: 3,
			last_name: 'Damar Treviño',
			avatar: ''
		} 

		]};
	},
	fetchWhatFuckingIs(it){
		switch(it){
			case 'evy':
				return {
					status: true,
					type: 'author',
					data: {
						html: 'this is author page:' + it
					}
				}
			break;

			case 'jobs':
			case 'creators':
			case 'about':
			case 'its-caturday':
				return {
					status: true,
					type: 'page',
					data: {
						html: 'this is page page:' + it
					}
				}
			break;

			case 'geek':
			case 'hips':
			case 'culture':
			case 'bit':
			case 'music':
			if(it === 'geek'){
				return {
					status: true,
					type: 'category',
					data: {
						html: 'this is category page special geek: <img data-id="15" src="https://i.pinimg.com/564x/fc/57/42/fc5742328ad5a62fa67dceb9e75c74a6.jpg" />' + it
					}
				}				
			}else{
				return {
					status: true,
					type: 'category',
					data: {
						html: 'this is category page:' + it
					}
				}
				}
			break;

			default:
				return {
					status: false,
					type: false,
					data: {
						html: null
					}
				}
			break;
		}
	},
	fetchPost(url){
		return axios.get(API_BASE_URL + 'api/'+ url.year + '-' + url.month + '-' + url.slug + '/post').then((res) => {
			return res.data;
		});
	}

};

export default PostAPI;