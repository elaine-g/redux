import axios from 'axios'

const API_BASE_URL = 'http://localhost:3000/';

export const fetchTag = (tag) => {
	return axios.get(API_BASE_URL + 'api/'+tag+'/tag').then((res) =>{
		return res.data
	})
}