import axios from 'axios'

const API_BASE_URL = 'http://localhost:3000/';

let SearchAPI = {
	get(query,filter){
		let order;
		if(typeof filter.order !== 'undefined'){
			if(filter.order === 'asc'){
				order = 'asc';
			}else if(filter.order === 'desc'){
				order = 'desc';
			}else{
				order = filter.order;
			}
			return axios.get(API_BASE_URL + 'api/search/'+ query +'/filter/'+ order).then((res) => {
				return res.data;
			});
		}
		return axios.get(API_BASE_URL + 'api/search/'+ query).then((res) => {
			return res.data;
		});
	}
};

export default SearchAPI;