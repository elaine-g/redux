import axios from 'axios'

const API_BASE_URL = 'http://localhost:3000/';

let HomeAPI = {
	fetchFeatured(){
		var url = API_BASE_URL + 'api/get/featured-content';
		return axios.get(url).then((response) => {
			return response.data;
		});
	}
};

export default HomeAPI;