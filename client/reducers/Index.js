import { combineReducers } from 'redux'
import ChannelReducer from './ChannelReducer'
import PostReducer from './PostReducer'
import {HeaderReducer} from './HeaderReducer'
import PageReducer from './PageReducer'
import HomeReducer from './HomeReducer'
import SearchReducer from './SearchReducer'
import RelatedReducer from './RelatedReducer'
import TagReducer from './TagReducer'
export default combineReducers({
  HeaderReducer,
  ChannelReducer,
  PostReducer,
  PageReducer,
  HomeReducer,
  SearchReducer,
  RelatedReducer,
  TagReducer
})