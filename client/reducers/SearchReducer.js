import update from 'immutability-helper'
import SEARCH_CONSTANTS from '../constants/SearchConstants/SearchConstants'

const initialState = {
	ui:{
		loading: false
	},
	error:{
		status: false,
		message: undefined
	},
	keyword: undefined,
	data: undefined
};

let SearchReducer = (nextState = initialState, action) =>{
	switch(action.type){

		case SEARCH_CONSTANTS.REQUEST_SEARCH_KEYWORD:
			return update(nextState,{ 
				ui:{
					loading: {$set: true}
				},
				keyword:{ $set: action.keyword }
			})
		break;

		case SEARCH_CONSTANTS.RECEIVE_SEARCH:
			return update(nextState,{
				ui:{ loading: { $set: false }},
				result:{ $set: action.result }
			})
		break;

		case SEARCH_CONSTANTS.REQUEST_SEARCH_KEYWORD_FAIL:
			return update(nextState,{
				ui:{ 
					loading:{ $set: false }, 
				},
					error:{ status:{ $set: true },message:{ $set: action.message } }
			})
		break;	

		case SEARCH_CONSTANTS.RESET_SEARCH:
			return update(nextState,{ $set: initialState })
		break;

		default:
			return nextState;
		break;
	}
}

export default SearchReducer