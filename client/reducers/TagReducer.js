import update from 'immutability-helper'
import TAG_CONSTANTS from '../constants/TagConstants/TagConstants'

const initialState = {
	ui:{
		loading: false
	},
	result:[],
	error:{
		status: false,
		message: 'undefined'
	}
}

let TagReducer = (nextState = initialState, action) =>{
	switch(action.type){
		case TAG_CONSTANTS.FETCH_TAG_REQUEST:
			return update(nextState,{ ui:{ loading: {$set: true} }, lastTag: { $set: action.payload }})
		break;

		case TAG_CONSTANTS.FETCH_TAG_SUCCESS:
			return update(nextState,{ ui:{loading:{$set:false}}, result: {$set: action.payload.result }})
		break;

		case TAG_CONSTANTS.FETCH_TAG_FAILURE:
			return update(nextState,{ error: { status:{$set:true}, message:{ $set: action.payload.message }}})
		break;	

		default:
			return nextState
		break;
	}
}

export default TagReducer