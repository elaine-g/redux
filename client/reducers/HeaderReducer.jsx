
import {ArtistConstant,SET_DROPDOWN} from '../constants/Constants'
import HEADER_CONSTANT from '../constants/HeaderConstants/HeaderConstants'

import update from 'immutability-helper';

const initialState = {
	ui:{
		sticky: false,
		dropdown:{
				open:false,
				last_action: undefined
		},
		
		isShow:false,
	},
	data: {
		categories: []
	}
};

export const HeaderReducer = (state = initialState,action) =>{
	console.log(action);
	switch(action.type){
		case HEADER_CONSTANT.SET_HEADER_STICKY:
			return update(state,{
				ui: {
					sticky:{$set:action.status}
					}
				});
		break;

		case HEADER_CONSTANT.SET_NAVBAR_CATEGORIES:
			return update(state,{data:{ categories: {$set:action.categories } }})
		break;

		case HEADER_CONSTANT.SET_DROPDOWN_HEADER:
			return update(state,{ui: {dropdown:{open:{$set:action.open},last_action:{$set:action.last_action}}}})	
		break;

		case 'reset header':
			return update(state,
				{
					$set:initialState
				})
		break;

		default:
			return state;
		break;
	}
};
