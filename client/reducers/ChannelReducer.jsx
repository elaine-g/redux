import CHANNEL_CONSTANTS from '../constants/ChannelConstants/ChannelConstants.jsx'
import update from 'immutability-helper'
const initialState = {}

const ChannelReducer = (state = initialState,action) =>{
	switch(action.type){
		case CHANNEL_CONSTANTS.SET_CURRENT_CHANNEL:
			return update(state,{
				current:{ $set: action.channel }
			})
		break;

		case CHANNEL_CONSTANTS.CHANGE_CHANNEL:
			return update(state,{
				current: { $set:action.channel }
			})
		break;

		case CHANNEL_CONSTANTS.RESET_CHANNEL:
			return update(state,{
				$unset: ['current']
			})
		break;

		default:
			return state;
		break;
	}
};

export default ChannelReducer;