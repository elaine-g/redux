import update from 'immutability-helper'
import PAGE_CONSTANTS from '../constants/PageConstants/PageConstants'

let initialState = {
	exists: false,
	type:{
		isPage: false,
		isChannel: false,
		isAuthor: false,
	},
	data: undefined,
	fetching: false
};

const PageReducer = (state = initialState,action) =>{
	switch(action.type){

		case PAGE_CONSTANTS.IS_PAGE:
			return update(state,{
				exists: { $set:action.exists },
				type:{
					isPage: { $set:action.exists },
					isChannel:{ $set:false },
					isAuthor:{ $set:false },
				},
				data  : { $set:action.data },
			});
		break;


		case PAGE_CONSTANTS.IS_CHANNEL:
			return update(state,{
				exists: { $set:action.exists },
				type:{
					isPage: { $set:false },
					isChannel:{ $set:action.exists },
					isAuthor:{ $set:false },
				},
				data  : { $set:action.data },
			});	
		break;

		case PAGE_CONSTANTS.IS_AUTHOR:
			return update(state,{
				exists: { $set:action.exists },
				type:{
					isPage: { $set:false },
					isChannel:{ $set:false },
					isAuthor:{ $set:action.exists },
				},
				data  : { $set:action.data },
			});	
		break;
		

		case PAGE_CONSTANTS.RESET:
			return update(state,{
				$set: initialState
			});
		break;

		case PAGE_CONSTANTS.NOT_FOUND:
			return update(state,{
				$set: initialState
			});			
		break;

		case PAGE_CONSTANTS.REQUEST_PAGE:
			return update(state,{
				fetching:{$set: true}
			});	
		break;

		case PAGE_CONSTANTS.REQUEST_PAGE_FINISHED:
			return update(state,{
				fetching:{$set: false}
			});	
		break;

		default:
			return state;
		break;
	}
};

export default PageReducer