import Constants from '../constants/Constants.jsx';
const initialState = {
	balance: 0,
	modified: false
};

const BankReducer =  (state = initialState,action) => {
	console.log(action);
	switch(action.type){
		case Constants.CREATE_ACCOUNT:
			return initialState;
		break;
		case Constants.DEPOSIT_INTO_ACCOUNT:
			return { balance : state.balance + parseFloat(action.amount), modified: action.modified };
		break;

		case Constants.WITHDRAW_FROM_ACCOUNT:
			if(state.balance < action.amount){
				return { balance: 0 , modified: action.modified };	
			}
			return { balance: state.balance - parseFloat(action.amount), modified: action.modified };
		
		break;	

		case Constants.RESET_ACCOUNT:
			return { balance: action.amount, modified: action.modified};
		break;

		default:
			return state;
		break;

	}
};

export default BankReducer;