import update from 'immutability-helper'
import HOME_CONSTANTS from '../constants/HomeConstants/HomeConstants'

let initialState = {
	featuredPost:{
		data: {
			featured: null,
			time : 0
		},
		featuredError: {
			status: false,
			message: null,
			code: false
		},
		ui:{
			loading: false
		}
	},
};

const HomeReducer = (nextState = initialState, action) =>{
	
	switch(action.type){

		case HOME_CONSTANTS.FETCH_FEATURED_POSTS_REQUEST:
			return update(nextState,{
				featuredPost: { ui: { loading: { $set: true } } }
			})
		break;

		case HOME_CONSTANTS.FETCH_FEATURED_POSTS_SUCCESS:
			return update(nextState,{
				featuredPost:{
					data:{
						time : { $set: action.payload.time },
						featured:{ $set: action.payload.featured }
					},
					featuredError:{
						status :{ $set: false },
						message:{ $set: null  },
						code: { $set: false }
					},
					ui: { loading: { $set: false } }
				},
			})		
		break;

		case HOME_CONSTANTS.FETCH_FEATURED_POSTS_FAILURE:
			return update(nextState,{
				featuredPost:{
					featuredError:{
						status :{ $set:true },
						message:{ $set: action.error.message  },
						code: { $set: action.error.code }
					},
					ui: { loading: { $set: false } }
				}
			})
		break;

		default:
			return nextState;
		break;
	}

};

export default HomeReducer;

