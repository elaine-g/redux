import update from 'immutability-helper'
import POST_CONSTANTS from '../constants/PostConstants/PostConstants'

let initialState = {
	date: {
		year : undefined,
		month: undefined,
		slug : undefined
	},
	post:{
		id: undefined,
		title: undefined,
		content: undefined,
		description: undefined,
	},
	ui:{
		loading: false
	},
	failure:{
		error:{
			status: false,
			message: undefined,
			code: false
		}
	},
	lastRequest: false
};

const PostReducer = (nextState = initialState,action) =>{
	switch(action.type){

		case POST_CONSTANTS.REQUEST_POSTS:
			return update(nextState,{ 
				ui: { loading: {$set:true} } ,
				lastRequest : {$set: action.payload}
			})
		break;

		case POST_CONSTANTS.RECEIVE_POSTS:
			return update(nextState,{ 
				date:{
					year: { $set: action.payload.result.date.year } ,
					month: { $set: action.payload.result.date.month } ,
					slug: { $set: action.payload.result.date.slug } , 
				},
				post:{
					id: {$set: action.payload.result.post.id},
					title: {$set: action.payload.result.post.title},
					content: {$set: action.payload.result.post.content},
					description: {$set: action.payload.result.post.description}
				},
				related:{
					$set: action.payload.result.related
				},
				failure:{
					error: {
						status: { $set: false },
						message: { $set: undefined }
					}
				},
				ui: { loading: {$set:false} } 
			})
		break;

		case POST_CONSTANTS.ERROR_RECEIVE_POSTS:
			return update(nextState,{
				failure:{
					error: {
						status: { $set: action.payload.error.status },
						message: { $set: action.payload.error.message },
						code: {$set: action.payload.error.code }
					}
				}
			})
		break;

		case POST_CONSTANTS.RESET_POSTS:
			return update(nextState,{
				$set: initialState
			});
		break;



		default:
			return nextState;
		break;
	}

};

export default PostReducer;

