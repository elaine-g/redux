import {ArtistConstant} from '../constants/Constants.jsx';
import update from 'immutability-helper';

const initialState = {
	img: 'xjpg',
	name: null,
	job: null,
	age: null,
	data:{
		last_name: null	,
		header: null,
		ready: false
	},
	error: false
	
};


const ArtistReducer =  (state = initialState,action) => {
	console.log(action);
	switch(action.type){

		case ArtistConstant.SHOW_INFORMATION:
			return { img: action.img, name: action.name ,job: action.job , age: action.age, isShow: action.isShow};
		break;

		case ArtistConstant.CHANGE_AGE:
		return Object.assign({}, state, {
		        age: action.age
		      })

		break;

		case ArtistConstant.GET_NAME:
		    return update(state,
		    	{data:{
		    			last_name: {$set:action.payload},
		    			ready: {$set: action.ready }
		    	}}
		    	)
		break;

		case ArtistConstant.ERROR_GET_NAME:
		return Object.assign({}, state, {
		        error: action.error
		      })
		break;

		case ArtistConstant.CHANGE_NAME_GEORGE:
			return update(state,
					    	{data:{
					    		last_name: {$set:action.payload}
					    	},
					    	error: false}
					    	)
		break;
		
		case ArtistConstant.GET_NAVBAR:
			return update(state,{data:{header: {$set:action.text}}})		
		break;

		default:
			return state;
		break;

	}
};

export default ArtistReducer;