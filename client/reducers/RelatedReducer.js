import update from 'immutability-helper'
import POST_RELATED_CONSTANTS from '../constants/PostConstants/PostRelatedConstants'
const initialState = {}

const RelatedReducer = (nexState = initialState,action) =>{

	switch(action.type){
		case POST_RELATED_CONSTANTS.REQUEST_RELATED_POSTS:
			return update(nexState,{
				loading: {$set:true}
			})
		break;
		default:
			return nexState;
		break;
	}

}

export default RelatedReducer