import axios from 'axios'
import PostAPI from './api/PostAPI'

const request = () => ({ type: 'REQUEST_POSTS' });
const receivedPost = (post) => ({ type: 'RECEIVE_POSTS', payload: post})

export const fetchMyPost = (url) => (dispatch, getState) => {
  dispatch(request());
  return PostAPI.fetchPost(url).then((res) =>{
  	dispatch(receivedPost(res.result));
  });
};