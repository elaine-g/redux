import express from 'express';
import cors from 'cors';
import moment from 'moment';
import API from './src/models/API.js';
const app = express();

app.use(cors());

app.set('view engine', 'ejs');
app.set('views', [ './server/src/views/pages']);

app.get('/',function(req,res){
	res.render('index');
});

app.get('/api/get/featured-content',function(req,res){
	API.fetchFeatured(function(response){
		res.status(200).json(response);
	});
});

app.get('/api/search/:query',function(req,res){
	var results = API.fakeSearch(req.params.query,req.params.filter);
	res.status(200).json(results);
});

app.get('/api/search/:query/filter/:filter',function(req,res){
	var results = API.fakeSearchWithFilter(req.params.query,req.params.filter);
	res.status(200).json(results);
});

app.get('/api/:year([0-9]{4})-:month([0-9]{2})-:slug/post',function(req,res){
	setTimeout(() => {
		return res.status(200).json({
		result: { 
			date:{
				year: req.params.year, 
				month: req.params.month, 
				slug: req.params.slug
			},
			post:{
				id: 1,
				title: 'Los tumblrs que pusieron el Internet de Cabeza',
				content: '<p>Así es</p><a href="http://localhost:8080/tag/paramore">paramore</a> rocket',
				description: 'Tumblr puso de cabeza al Internet'
			},
			related: [
			{
				title : 'Parahoy cruise3',
				link  : '/music/2018-04-parahoy-cruise3'
			},
			{
				title : 'Fornite epic games',
				link  : '/games/2018-04-fornite-presenta-nuevas-actualizaciones'
			},
			{
				title : 'Yarilo',
				link  : '/music/2012-09-arkona-ciudades'
			}
		]
		}
		});
		},0)

});

app.get('/api/:tag/tag',function(req,res){
	if(req.params.tag.length > 0){
		res.status(404).json({status:true,result:[{ id: 1, title: req.params.tag + ' tag' }]})
	}
});	

app.get('/assets/ES-MX.json',function(req,res){
	res.status(200).json({
		"ADRESS" : {
			"TITLE" : "¿A dónde llevamos tu pedido?"
		}
	});
});
app.listen(3000,function(){
	console.log(' server is running ');
});