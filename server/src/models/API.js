import moment from 'moment';
var API = {
	fetchFeatured(callback){
		var featured = {
		status: true,
		time: moment().unix(),
		featured:
		{
			leftSide:		
			[
			{
				id:1,
				title: 'Hard Times nuevo sencillo de Paramorex2.',
				description: 'PARAMORE inicia año estrenando...',
				cover:{
					thumbnail: 'https://cdn.playground.do/embed/0b9cf6e7b4193030b933044371521745867/320_dreide.jpg',
					original: ''
				},
				url: '/music/2018-04-hard-times-nuevo-sencillo-paramore',
				slug: '/hard-times-nuevo-sencillo-paramore'
			},
			{
				id:2,
				title: 'Evy Rosas nos da una entrevistax.',
				description: 'La famosa Social Manager de Game planet nos...',
				cover:{
					thumbnail: 'https://i.pinimg.com/564x/40/5d/4b/405d4b2ca827949b5489c0146202dea1.jpg',
					original: ''
				},
				url: '/geek/2018-04-evy-rosas',
				slug: 'evy-rosas'
			}
			],
			rightSide:[
			{
				id:3,
				title: 'Hard Times nuevo sencillo de Paramorex2.',
				description: 'PARAMORE inicia año estrenando...',
				cover:{
					thumbnail: 'https://i.pinimg.com/564x/6f/4e/9c/6f4e9ce215330a8958a6e1b677de02a4.jpg',
					original: ''
				},
				url: '/music/2018-04-hard-times-nuevo-sencillo-paramore-x2',
				slug: '/evy-rosas-entrevista'
			},
			{
				id:4,
				title: 'Hard Times nuevo sencillo de Paramorex2.',
				description: 'PARAMORE inicia año estrenando...',
				cover:{
					thumbnail: 'https://cdn.playground.do/embed/0b9cf6e7b4193030b933044371521745867/320_dreide.jpg',
					original: ''
				},
				url: '/music/2018-04-hard-times-nuevo-sencillo-paramore-x2',
				slug: '/hard-times-nuevo-sencillo-paramore-x2'
			}
			]
		}

		};
		return callback(featured);
		/*setTimeout(function(){
			return callback(featured);
		},0);
		*/
	},
	fakeSearchWithFilter(query,filter){
		var filtersAllowed = ['asc','desc'];
		if(filtersAllowed.indexOf(filter) !== -1){
			var result = {
				status: true,
				data:{
					results:[
					{ title: query + ' search example', description: '', link: ''},
					{ title: query + ' search example 2', description: '', link: ''},
					]
				}
			};
			return result;
		}
		return { status: false, message: 'filter doesnt exists'};
	},
	fakeSearch(query){
		var result = {
			status: true,
			data:{
				results:[
				{ title: query + ' search example without filter', description: '', link: ''},
				{ title: query + ' search example 2 without filter', description: '', link: ''},
				]
			}
		};
		return result;		
	}
};


module.exports = API;